package com.nghiapv;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by nghiapv on 21/12/2016.
 */
@Controller
public class WelcomeController {

    @Value("${welcome.message:test}")
    private String message = "Hello World";

    @RequestMapping("/")
    public String Welcome(Map<String, Object> model){
        model.put("message", this.message);
        return "welcome";
    }
}
